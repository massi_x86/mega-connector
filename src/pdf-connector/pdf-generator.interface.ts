import { Content } from '../dto/structure.dto';

export interface PDFGenerator {
  generatePDF(content: Content[]) : PDFKit.PDFDocument
}

