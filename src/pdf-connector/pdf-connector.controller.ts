import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Header,
  HttpCode, Inject,
  Logger,
  Post,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { constants } from 'http2';
import { ApiTags } from '@nestjs/swagger';
import { StructureDto } from '../dto/structure.dto';
import { Response } from 'express';
import { PDFGenerator } from './pdf-generator.interface';

@ApiTags('pdf')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('/v1/pdf')
export class PDFConnectorController {
  constructor(@Inject('PDFGenerator') private service: PDFGenerator) {}

  @Post('/new')
  @HttpCode(constants.HTTP_STATUS_CREATED)
  @Header(constants.HTTP2_HEADER_CACHE_CONTROL, 'None')
  @Header(constants.HTTP2_HEADER_CONTENT_TYPE, 'application/pdf')
  public async createPdf(@Body() structureDto: StructureDto, @Res() res: Response) {
    Logger.log('Creating new PDF');

    const generatedDoc = this.service.generatePDF(structureDto.content);
    generatedDoc.pipe(res);
  }
}

