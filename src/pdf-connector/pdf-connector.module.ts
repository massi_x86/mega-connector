import { Module } from '@nestjs/common';
import { PDFConnectorController } from './pdf-connector.controller';
import { PDFKitGeneratorService } from './pdfkit-generator.service';

@Module({
  providers: [{ provide: 'PDFGenerator', useClass: PDFKitGeneratorService }],
  controllers: [PDFConnectorController],
})
export class PDFConnectorModule {}
