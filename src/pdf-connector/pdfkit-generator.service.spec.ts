import { Test, TestingModule } from '@nestjs/testing';
import { PDFKitGeneratorService } from './pdfkit-generator.service';
import exp = require('constants');

describe('PDFKitGeneratorService', () => {
  let service: PDFKitGeneratorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PDFKitGeneratorService],
    }).compile();

    service = module.get<PDFKitGeneratorService>(PDFKitGeneratorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should generate a pdf', () => {
    expect(service.generatePDF([])).toBeDefined();
  });

  it('the document should be final', () => {
    const doc = service.generatePDF([]);
    expect(doc.end).toThrowError('Cannot read property');
  });
});
