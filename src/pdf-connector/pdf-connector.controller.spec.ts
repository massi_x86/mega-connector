import { Test, TestingModule } from '@nestjs/testing';
import { PDFConnectorController } from './pdf-connector.controller';
import { Injectable } from '@nestjs/common';
import { Content } from '../dto/structure.dto';
import { PDFGenerator } from './pdf-generator.interface';
import * as PDFDocument from 'pdfkit';

@Injectable()
export class FakePDFGeneratorService implements PDFGenerator {

  generatePDF(content: Content[]): PDFKit.PDFDocument {
    const doc = new PDFDocument();
    doc.text('Some text');
    doc.addPage();
    doc.text('Some other text');
    doc.end();
    return doc;
  }
}

describe('PDFConnectorController', () => {
  let controller: PDFConnectorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PDFConnectorController],
      providers: [{ provide: 'PDFGenerator', useClass: FakePDFGeneratorService }]
    }).compile();

    controller = module.get<PDFConnectorController>(PDFConnectorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

