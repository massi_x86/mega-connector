import { Injectable } from '@nestjs/common';
import * as PDFDocument from 'pdfkit';
import { Content } from '../dto/structure.dto';
import { PDFGenerator } from './pdf-generator.interface';

@Injectable()
export class PDFKitGeneratorService implements PDFGenerator{

  public generatePDF(content: Content[]): PDFKit.PDFDocument {
    const doc = new PDFDocument();
    doc.text('Automatically generated file');

    content.forEach(c => {
      doc.text(`${c.name} (${c.type})`);
      doc.image(c.content, { width: 500 });
      doc.addPage();
    })

    // Forbid further modifications from here
    doc.end();

    return doc;
  }
}

