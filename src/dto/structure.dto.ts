import { ApiProperty } from '@nestjs/swagger';

export class Content {

  @ApiProperty({ required: false })
  name?: string;

  @ApiProperty({ required: false })
  type?: string;

  @ApiProperty( { required: true, description: 'DataURI Encoded' })
  content: string;
}


export class StructureDto {
  @ApiProperty({ required: false })
  id?: number;

  @ApiProperty({ required: false })
  name?: string;

  @ApiProperty( { required: true, type: [Content] })
  content: Content[]
}
