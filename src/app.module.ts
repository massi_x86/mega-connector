import { Module } from '@nestjs/common';
import { PDFConnectorModule } from './pdf-connector/pdf-connector.module';

@Module({
  imports: [
    PDFConnectorModule,
  ],
})
export class AppModule {}
