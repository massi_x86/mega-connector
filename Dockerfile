FROM node:14-alpine as build

RUN mkdir /code
WORKDIR /code

ADD package.json /code
ADD package-lock.json /code
ADD tsconfig.json /code
ADD tsconfig.build.json /code

ADD src /code

RUN npm i
RUN npm run build


FROM node:14-alpine
WORKDIR /run
COPY --from=build /code/dist /run/dist
COPY --from=build /code/node_modules /run/node_modules

ADD package.json /run
ADD package-lock.json /run

CMD ["npm", "run", "start:prod"]

